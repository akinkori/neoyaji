//
//  AppDelegate.h
//  neoyaji
//
//  Created by kinkori on 13/08/24.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import <GameFeatKit/GFController.h>
#import "Bead.h"
#import <AidAd/AidAd.h>

@interface AppController : NSObject <UIApplicationDelegate, CCDirectorDelegate, AidAdDialogBlocker>
{
	UIWindow *window_;
	UINavigationController *navController_;

	CCDirectorIOS	*director_;							// weak ref
}

@property (nonatomic, retain) UIWindow *window;
@property (readonly) UINavigationController *navController;
@property (readonly) CCDirectorIOS *director;

@end
