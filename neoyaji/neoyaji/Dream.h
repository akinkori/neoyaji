//
//  Dream.h
//  neoyaji
//
//  Created by 蔭山 忠臣 on 2013/10/04.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Dream : CCSprite {
    NSArray *clouds;
    int h_index;
    CCLabelBMFont *score;
    CCSprite *hero;
    CCSprite *bk;
}

-(void) returnFace;
-(void) changeFace;
-(void) score_fadeout;

@property(nonatomic, retain) NSMutableArray *touched_heros;

@end
