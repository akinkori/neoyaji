//
//  GameLayer.h
//  neoyaji
//
//  Created by kinkori on 13/08/24.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Oyaji.h"
#import "NoseBalloon.h"
#import "Grid.h"
#import <Social/Social.h>
#import "Bead.h"
#import <AidAd/AidAd.h>


@interface GameLayer : CCLayer {
    int stage_index;
    float cloud_interval;
    float myscale;
    CGSize win_size;
    CCProgressTimer *progress_timer;
    Oyaji *oyaji;
    NoseBalloon *balloon;
    CCSprite *bkg;
    CCSprite *sky;
    CCLayer *sky_layer;
    CCLayer *bkg_layer;
    CCLayer *clowd_layer;
    CCLayer *oyaji_layer;
    CCLayer *dream_layer;
    CCSprite *rainbow;
    CCSprite *plane_cloud;
    CCSprite *firework;
    CCSprite *color_balloon;
    NSArray *bkg_lis;
    BOOL playing_game;
    CCSprite *timing;
    CCSprite *point_bkg;
    CCSprite *closeupline;
    int score;
    UIViewController *viewController;
}

+(CCScene *) scene;
-(void) add_score:(float)difference big:(BOOL)is_big;
-(NSString *)cast:(float) remain;
-(void)setresultMenu;
-(void) score_add_from_hero:(NSNotification *)center;
@property (nonatomic, retain) CCProgressTimer *progress_timer;
@property (nonatomic, retain) Oyaji *oyaji;
@property (nonatomic, retain) CCSprite *bkg;
@property (nonatomic, retain) NoseBalloon *balloon;
@property (nonatomic, retain) NSArray *bkg_lis;
@property (nonatomic, retain) NSArray *sky_lis;
@property (nonatomic, retain) NSArray *bgm_lis;
@property (nonatomic, retain) Grid *grid;
@property (nonatomic, retain) CCSprite *timeframe;
@property (nonatomic, retain) CCLabelBMFont *label;
@property (nonatomic, retain) CCLabelBMFont *stage_label;
@property (nonatomic, retain) CCLayer *hero_layer;
@property (nonatomic, retain) CCLayer *dream_layer;
@property (nonatomic, retain) NSMutableArray *touched_heros;
@property (nonatomic, retain) NSMutableArray *each_score;
@property (nonatomic, retain) NSString *score_bkg_;

@end
