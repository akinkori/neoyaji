//
//  AppDelegate.m
//  neoyaji
//
//  Created by kinkori on 13/08/24.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//

#import "cocos2d.h"
#import <GameKit/GameKit.h>
#import "AppDelegate.h"
#import "IntroLayer.h"
#import "SimpleAudioEngine.h"
#import "GAI.h"

#define kMEDIA_CODE  @"idxxxxxxxxxoWgn"

@implementation AppController

@synthesize window=window_, navController=navController_, director=director_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-43468245-3"];
    [[GAI sharedInstance] setDefaultTracker:tracker];
	// Google analytics の設定
    
     AidAdAgent* aaa = [AidAd agentForMedia:kMEDIA_CODE];
    [aaa setDialogBlocker:self]; //自分自身をブロッカーとして設定
    [aaa startLoading];
    
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"mini_explosion.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"swing_renzoku.wav"];
    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"fall_street1.wav"];
    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"Town_afternoon.mp3"];
    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"evening.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"key_lock.mp3"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"Crrect_answer3.mp3"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"Crrect_answer1.mp3"];
	// Create the main window
	window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];


	// Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits
	CCGLView *glView = [CCGLView viewWithFrame:[window_ bounds]
								   pixelFormat:kEAGLColorFormatRGB565	//kEAGLColorFormatRGBA8
								   depthFormat:0	//GL_DEPTH_COMPONENT24_OES
							preserveBackbuffer:NO
									sharegroup:nil
								 multiSampling:NO
							   numberOfSamples:0];
    [glView setMultipleTouchEnabled:YES];

	director_ = (CCDirectorIOS*) [CCDirector sharedDirector];

	director_.wantsFullScreenLayout = YES;

	// Display FSP and SPF
	[director_ setDisplayStats:NO];

	// set FPS at 60
	[director_ setAnimationInterval:1.0/60];

	// attach the openglView to the director
	[director_ setView:glView];

	// for rotation and other messages
	[director_ setDelegate:self];

	// 2D projection
	[director_ setProjection:kCCDirectorProjection2D];
//	[director setProjection:kCCDirectorProjection3D];

	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director_ enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");

	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

	// If the 1st suffix is not found and if fallback is enabled then fallback suffixes are going to searched. If none is found, it will try with the name without suffix.
	// On iPad HD  : "-ipadhd", "-ipad",  "-hd"
	// On iPad     : "-ipad", "-hd"
	// On iPhone HD: "-hd"
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
	[sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"

	// Assume that PVR images have premultiplied alpha
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];

	// and add the scene to the stack. The director will run it when it automatically when the view is displayed.
	[director_ pushScene: [IntroLayer scene]]; 

    [self authenticateLocalPlayer];
	
	// Create a Navigation Controller with the Director
	navController_ = [[UINavigationController alloc] initWithRootViewController:director_];
	navController_.navigationBarHidden = YES;
	
	// set the Navigation Controller as the root view controller
//	[window_ addSubview:navController_.view];	// Generates flicker.
	[window_ setRootViewController:navController_];
	
	// make main window visible
	[window_ makeKeyAndVisible];
	
	return YES;
}

// Supported orientations: Landscape. Customize it for your own needs
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


// getting a call, pause the game
-(void) applicationWillResignActive:(UIApplication *)application
{
	if( [navController_ visibleViewController] == director_ )
		[director_ pause];
}

// call got rejected
-(void) applicationDidBecomeActive:(UIApplication *)application
{
     [GFController activateGF:@"2182"];
	if( [navController_ visibleViewController] == director_ )
		[director_ resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
    UIDevice *device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    if ([device respondsToSelector:@selector(isMultitaskingSupported)]) {
        backgroundSupported = device.multitaskingSupported;
    }
    if (backgroundSupported) {
        [GFController backgroundTask];
    }
	if( [navController_ visibleViewController] == director_ )
		[director_ stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
    [GFController conversionCheckStop];
	if( [navController_ visibleViewController] == director_ )
		[director_ startAnimation];
}

// application will be killed
- (void)applicationWillTerminate:(UIApplication *)application
{
	CC_DIRECTOR_END();
}

// purge memory
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void) dealloc
{
	[window_ release];
	[navController_ release];

	[super dealloc];
}

- (BOOL)shouldBlockDialog:(AidAdAgent*)aaa
{
    // ダイアログ表示試行回数を3で割った余が0ではない(1,2, 4,5,..回目)とき
    if ([aaa countAttemptsToShowDialog] % 5 != 0) {
        NSLog(@"dialog blocked");
        return YES;  //ダイアログ表示をブロックする
    }
    return NO; //それ以外はブロックしない
}


-(void)authenticateLocalPlayer {
    [[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:^(NSError *error) {
        if (error)
        {
            NSLog(@"%@", error);
            /* エラー処理 */ }
        else
        { /* 認証済みユーザーを使ってハイスコアとか処理 */
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:@"neoyaji"] autorelease];
            int touch_count = [defaults integerForKey:@"missed_highscore"];
            int highscore = [defaults integerForKey:@"highscore"];
            if (touch_count && touch_count >= highscore) {
                scoreReporter.value = touch_count;
                [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
                    if (error != nil)
                    {
                        [defaults removeObjectForKey:@"missed_highscore"];
                    }
                    else {
                    }
                }];
            }
            
        }
    }];
}

@end

