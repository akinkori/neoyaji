//
//  NoseBalloon.h
//  neoyaji
//
//  Created by kinkori on 13/08/24.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface NoseBalloon : CCSprite {
    
}

-(id)init:(int)index;
-(CCSequence *)get_sequence:(int)index;
-(void) break_balloon;

@end
