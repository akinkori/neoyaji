//
//  ReleaseLayer.m
//  neoyaji
//
//  Created by kinkori on 13/09/20.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "ReleaseLayer.h"
#import "SimpleAudioEngine.h"


@implementation ReleaseLayer

-(id)init:(int)index {
    
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    
    myscale = 1.0;
    if(![Display isRetinaDevice]){
        myscale = 0.5;
    }
    
    if(self = [super init]) {
        
        bkg = [[CCSprite alloc] initWithFile:@"new_commer.png"];
        bkg.position = ccp(win_size.width /2, win_size.height/2);
        bkg.scale = myscale;
        [self addChild:bkg];
        /* 新キャラ追加用のバックグラウンド */
        
        NSBundle* bundle = [NSBundle mainBundle];
        NSString* path = [bundle pathForResource:@"heros" ofType:@"plist"];
        NSMutableArray *heros = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:path]];
        NSString *next_hero_name = heros[index];
        NSString* score_path = [bundle pathForResource:@"scores" ofType:@"plist"];
        NSArray *scores = [NSArray arrayWithContentsOfFile:score_path];
        NSString *next_hero_score = scores[index];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         [NSString stringWithFormat:@"%@.plist", next_hero_name]];
        
        NSString *release_cond_path = [bundle pathForResource:@"release_cond" ofType:@"plist"];
        NSArray *release_conds = [NSArray arrayWithContentsOfFile:release_cond_path];
        /* 開放条件のリスト */
        int cond_index = [[NSUserDefaults standardUserDefaults] integerForKey:@"release_cond_index"];
        if(!cond_index) {
            cond_index = 0;
        }
        /* 開放条件の番号 */
        
        NSDictionary *release_cond = release_conds[cond_index];
        if([self passed_cond:release_cond]) {
            [[SimpleAudioEngine sharedEngine] playEffect:@"Crrect_answer3.mp3"];
            
            
            /* 開放条件を満たしている場合の処理 */
            CCSprite *hero = [[CCSprite alloc ] initWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:[NSString stringWithFormat:@"%@_%d.png", next_hero_name, 0]]];
            hero.position = ccp(win_size.width/2 + 92, win_size.height/2 + 23);
            [self addChild:hero];
            
            /* スコアを表示する */
            CCLabelBMFont *score_label = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"+%@", next_hero_score] fntFile:@"number_hade.fnt"];
            score_label.anchorPoint = ccp(0.5, 1);
            score_label.position = ccp(win_size.width/2 + 92, win_size.height/2 - 2);
            score_label.scale = 0.5;
            [self addChild:score_label];
            
            
            [[NSUserDefaults standardUserDefaults]  setInteger:cond_index+1 forKey:@"release_cond_index"];
            [[NSUserDefaults standardUserDefaults] setInteger:index forKey:@"released_hero"];
            
            if(heros.count == ++index){
                CCSprite *releaseLabel = [CCSprite spriteWithFile:@"complete.png"];
                releaseLabel.position = ccp(win_size.width/2 + 95, win_size.height/2 + 70);
                releaseLabel.scale = myscale;
                [self addChild:releaseLabel];
            }else{
                CCSprite *releaseLabel = [CCSprite spriteWithFile:@"release.png"];
                releaseLabel.position = ccp(win_size.width/2 + 95, win_size.height/2 - 53);
                releaseLabel.scale = myscale;
                [self addChild:releaseLabel];
            }
        }else {
            CCSprite *hero = [[CCSprite alloc ] initWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:[NSString stringWithFormat:@"%@_%d.png", next_hero_name, 2]]];
            hero.position = ccp(win_size.width/2 + 95, win_size.height/2 + 10);
            [self addChild:hero];
        }
        goal.anchorPoint = ccp(0, 1);
        goal.position = ccp(win_size.width/2 - 140, win_size.height/2  + 40);
        [self addChild:goal];
        
        achievement_ratio = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d/%d", get_count, target_count]
                                               fontName:@"Arial-BoldMT" fontSize:20];
        if(score_hurdle){
            NSString *score_ratio = [NSString stringWithFormat:@"%d/%d", [[NSUserDefaults standardUserDefaults] integerForKey:@"now_get_count"], [[release_cond objectForKey:@"score"] intValue]];
            [achievement_ratio setString:[NSString stringWithFormat:@"%@    %@", achievement_ratio.string, score_ratio]];
        }
        
        achievement_ratio.position = ccp(win_size.width/2 -55, win_size.height/2 - 35);
        achievement_ratio.color = ccc3(0, 0, 0);
        [self addChild:achievement_ratio];
        
        CCMenuItem *closeMenuItem = [CCMenuItemImage itemFromNormalImage:@"close_btn.png" selectedImage:@"close_btn_on.png" target:self selector:@selector(close_layer)];
        closeMenuItem.scale = myscale;
        closeMenuItem.position = ccp(win_size.width/2, win_size.height/2 - 80);
        
        CCMenu *startMenu = [CCMenu menuWithItems:closeMenuItem, nil];
        startMenu.position = CGPointZero;
        [self addChild:startMenu];
        
    }
    return self;
}

-(BOOL) passed_cond:(NSDictionary*)cond_dic {
    NSString *cond_type = (NSString *)[cond_dic objectForKey:@"cond"];
    goal.scale = myscale;
    goal = [CCLabelTTF labelWithString:[cond_dic objectForKey:@"text"] dimensions:CGSizeMake(170, 200) alignment:UITextAlignmentLeft fontName:@"Arial-BoldMT" fontSize:15];
    goal.color = ccc3(0, 0, 0);
    if([cond_type isEqualToString:@"play"]) {
        get_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"get_count"];
        if(!get_count) {
            get_count = 1;
        }
        target_count = [[cond_dic objectForKey:@"count"] intValue];
        
        return get_count >= target_count;
    }
    else if([cond_type isEqualToString:@"score"]) {
        get_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"now_get_count"];
        target_count = [[cond_dic objectForKey:@"count"] intValue];
        return get_count >= target_count;
        
    }
    else if ([cond_type isEqualToString:@"same_heros"]) {
        NSArray *touch_arr = [[NSUserDefaults standardUserDefaults] arrayForKey:@"touched_heros"];
        get_count = 0;
        target_count = [[cond_dic objectForKey:@"count"] intValue];
        int target_score = [[cond_dic objectForKey:@"score"] intValue];
        int get_score = [[NSUserDefaults standardUserDefaults] integerForKey:@"now_get_count"];
        NSString *hero_name = [cond_dic objectForKey:@"name"];
        for(int i =0; i < [touch_arr count]; i++) {
            if([hero_name isEqualToString:touch_arr[i]]) {
                get_count++;
            }
        }
        score_hurdle = YES;
        return (get_count >= target_count && get_score >= target_score );
    }
    else if ([cond_type isEqualToString:@"hero_count"]) {
        NSArray *touch_arr = [[NSUserDefaults standardUserDefaults] arrayForKey:@"touched_heros"];
        get_count = [touch_arr count];
        target_count = [[cond_dic objectForKey:@"count"] intValue];
        int target_score = [[cond_dic objectForKey:@"score"] intValue];
        int get_score = [[NSUserDefaults standardUserDefaults] integerForKey:@"now_get_count"];
        score_hurdle = YES;
        return (get_count >= target_count && get_score >= target_score );
    }else if ([cond_type isEqualToString:@"each_score"]) {
        NSArray *score_arr = [[NSUserDefaults standardUserDefaults] arrayForKey:@"each_score"];
        target_count = [[cond_dic objectForKey:@"count"] intValue];
        int hurdle = [[cond_dic objectForKey:@"hurdle"] intValue];
        get_count = 0;
        for(id score in score_arr){
            if([(NSString *)score intValue] > hurdle){
                get_count++;
            }
        }
        return get_count >= target_count;
    }else {
        return NO;
    }
    
}

-(void) close_layer {
    [[SimpleAudioEngine sharedEngine] playEffect:@"key_lock.mp3"];
    [self removeFromParentAndCleanup:YES];
}


@synthesize release_image;
@synthesize bkg;
@synthesize goal;
@synthesize target_count;
@synthesize score_hurdle;
@synthesize get_count;
@synthesize achievement_ratio;
@synthesize myscale;

@end
