//
//  StartLayer.m
//  oyajimegane
//
//  Created by 蔭山 忠臣 on 2013/07/06.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "SimpleAudioEngine.h"
#import "StartLayer.h"
#import "AppDelegate.h"
#import "Display.h"
#import "GameLayer.h"
#import "DescriptionLayer.h"


@implementation StartLayer



// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	StartLayer *layer = [StartLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
    
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
        
        /* 画面サイズの取得 */
        CGSize win_size = [[CCDirector sharedDirector] winSize];
        myscale = 0.5;
        if([Display isRetinaDevice]){
            myscale = 1.0;
        }
        /* 画面サイズの取得 */
        
        /* 背景 */
        NSString *startimg = @"start_bkg.jpg";
        CCSprite *background = [CCSprite spriteWithFile:startimg];
        background.scale = myscale;
        background.position = ccp(win_size.width/2, win_size.height/2);
        [self addChild: background];
        /* 背景 */
        
        
        /* ボタンの作成 */
        
        CCMenuItem *startMenuItem = [CCMenuItemImage itemWithNormalImage:@"start_btn.png" selectedImage:@"start_btn_on.png" target:self selector:@selector(transition_game)];
        startMenuItem.scale = myscale;
		startMenuItem.position = ccp(win_size.width/2, 45);
        
        CCMenuItem *moreAppMenuItem = [CCMenuItemImage itemWithNormalImage:@"moreapp_btn.png" selectedImage:@"moreapp_btn_on.png" target:self selector:@selector(onClickBtn)];
        moreAppMenuItem.scale = myscale;
		moreAppMenuItem.position = ccp(win_size.width - 90, 45);
        
        CCMenuItem *rankingMenuItem = [CCMenuItemImage itemWithNormalImage:@"rank_btn.png" selectedImage:@"rank_btn_on.png" target:self selector:@selector(showLeaderboard)];
        rankingMenuItem.scale = myscale;
        rankingMenuItem.position = ccp(90, 45);
        	
    
        CCMenu *startMenu = [CCMenu menuWithItems:startMenuItem, moreAppMenuItem, rankingMenuItem, nil];
        startMenu.position = CGPointZero;
        [self addChild:startMenu];
        
        gfIconController = [[GFIconController alloc] init];
        
        // アイコンの自動更新間隔を指定（デフォルトで30秒／最短10秒）
        [gfIconController setRefreshTiming:20];
        
        // アイコン下のアプリ名テキストの色をUIColorで指定出来ます（デフォルト黒）
        [gfIconController setAppNameColor:[UIColor redColor]];
        
        // アイコンの配置位置を設定（1個〜20個まで設置出来ます）
        gfIconController = [[GFIconController alloc] init];
        
        // アイコンの自動更新間隔を指定（デフォルトで30秒／最短10秒）
        [gfIconController setRefreshTiming:200];
        
        // アイコン下のアプリ名テキストの色をUIColorで指定出来ます（デフォルト黒）
        [gfIconController setAppNameColor:[UIColor redColor]];
        
        // アイコンの配置位置を設定（1個〜20個まで設置出来ます）
        float rect_size = 57;
        iconView = [[[GFIconView alloc] initWithFrame:CGRectMake(50, win_size.height/2 -70, rect_size, rect_size)] autorelease];
        iconView2 = [[[GFIconView alloc] initWithFrame:CGRectMake(win_size.width - 110, win_size.height/2 -70, rect_size, rect_size)] autorelease];
        
        [gfIconController addIconView:iconView];
        [gfIconController addIconView:iconView2];
        [[CCDirector sharedDirector].view addSubview:iconView];
        [[CCDirector sharedDirector].view addSubview:iconView2];
         [gfIconController loadAd:@"2182"];

        /* ボタンの作成 */
    }
    
	return self;
}

- (void) retrieveTopThreeScores
{
    GKLocalPlayer *lp = [GKLocalPlayer localPlayer];
    NSString *lp_id = [lp playerID];
    NSString *my_name = [lp displayName];
    
    GKLeaderboard *leaderboardRequest = [[GKLeaderboard alloc] init];
    if (leaderboardRequest != nil)
    {
        leaderboardRequest.playerScope = GKLeaderboardPlayerScopeGlobal;
        leaderboardRequest.timeScope = GKLeaderboardTimeScopeAllTime;
        leaderboardRequest.category = @"oyajimegane";
        leaderboardRequest.range = NSMakeRange(1,3);
        [leaderboardRequest loadScoresWithCompletionHandler: ^(NSArray *scores,
                                                               NSError *error) {
            if (error != nil)
            {
                NSLog(@"error");
                // エラーを処理する。
            }
            if (scores != nil)
            {
                for(int i=0; i<scores.count; i++){
                    GKScore *rank_score = (GKScore *)scores[i];
                    if([[rank_score playerID] isEqualToString:lp_id]) {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setInteger:i+1 forKey:@"myrank"];
                        [self call_setCrown];
                    }else{
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setInteger:100 forKey:@"myrank"];
                    }
                }
                // スコア情報を処理する。
            }
        }];
        
    }
    
    GKLeaderboard *ranbuleaderboardRequest = [[GKLeaderboard alloc] init];
    if (ranbuleaderboardRequest != nil) {
        
        ranbuleaderboardRequest.playerScope = GKLeaderboardPlayerScopeGlobal;
        ranbuleaderboardRequest.timeScope = GKLeaderboardTimeScopeAllTime;
        ranbuleaderboardRequest.category = @"oyajimegane_ranbu";
        ranbuleaderboardRequest.range = NSMakeRange(1,3);
        [ranbuleaderboardRequest loadScoresWithCompletionHandler: ^(NSArray *scores,
                                                                    NSError *error) {
            if (error != nil)
            {
                NSLog(@"error");
                // エラーを処理する。
            }
            if (scores != nil)
            {
                for(int i=0; i<scores.count; i++){
                    GKScore *rank_score = (GKScore *)scores[i];
                    if([[rank_score playerID] isEqualToString:lp_id]) {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setInteger:i+1 forKey:@"ranburank"];
                        [self call_setCrown];
                    }else{
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setInteger:100 forKey:@"ranburank"];
                    }
            }
                // スコア情報を処理する。
            }
        }];
        
    }
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

#pragma mark GameKit delegate
-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
/*
 AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
 */
}

-(void)transition_game {
    [[SimpleAudioEngine sharedEngine] playEffect:@"key_lock.mp3"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [gfIconController stopAd];
    UIView *view = [CCDirector sharedDirector].view;
    for (UIView* subview in view.subviews) {
        [subview removeFromSuperview];
    }
    int highscore = [defaults integerForKey:@"highscore"];
    if (!highscore) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[DescriptionLayer scene] withColor:ccBLACK]];
    }else{
       [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameLayer scene]withColor:ccBLACK]];
    }
    
}

-(void)showLeaderboard
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"key_lock.mp3"];
    [[CCDirector sharedDirector] stopAnimation];
    GKLeaderboardViewController *leaderboardViewController = [[[GKLeaderboardViewController alloc] init] autorelease];
    if (leaderboardViewController != nil) {
        // カテゴリ指定を解除
    leaderboardViewController.category = nil;
    leaderboardViewController.leaderboardDelegate = self;
    
    AppController *app=(AppController*)[UIApplication sharedApplication].delegate;
    [app.navController presentModalViewController:leaderboardViewController animated:YES];
    }
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    AppController *app=(AppController*)[UIApplication sharedApplication].delegate;
    [app.navController  dismissModalViewControllerAnimated:YES];
    
    [[CCDirector sharedDirector] startAnimation];
}


- (void)didShowGameFeat{
    // GameFeatが表示されたタイミングで呼び出されるdelegateメソッド
    NSLog(@"didShowGameFeat");
}
- (void)didCloseGameFeat{
    // GameFeatが閉じられたタイミングで呼び出されるdelegateメソッド
    NSLog(@"didCloseGameFeat");
}

- (void)onClickBtn{
    [[SimpleAudioEngine sharedEngine] playEffect:@"key_lock.mp3"];
    [GFController showGF:[CCDirector sharedDirector] site_id:@"2182" delegate:self];
}

@synthesize iconView;
@synthesize iconView2;


@end
