//
//  ReleaseLayer.h
//  neoyaji
//
//  Created by kinkori on 13/09/20.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Display.h"

@interface ReleaseLayer : CCLayer {
    
}
@property(nonatomic, retain) CCSprite *bkg;
@property(nonatomic, retain) CCSprite *release_image;
@property(nonatomic, retain) CCLabelTTF *goal;
@property(nonatomic, retain) CCLabelTTF *achievement_ratio;
@property int target_count;
@property int get_count;
@property BOOL score_hurdle;
@property float myscale;

-(id)init:(int)index;
-(void) close_layer;

@end
