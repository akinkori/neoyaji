//
//  Clowd.h
//  neoyaji
//
//  Created by 蔭山 忠臣 on 2013/09/14.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Clowd : CCSprite {
    float speed;
}

-(void) setOutside;
-(void) setRandomPosition;
-(void) setRandomSpeed;
-(void) slide;
-(void)check_is_out;

@end
