//
//  DescriptionLayer.m
//  neoyaji
//
//  Created by 蔭山 忠臣 on 2013/09/21.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "DescriptionLayer.h"
#import "GameLayer.h"
#import "Display.h"
#import "SimpleAudioEngine.h"



@implementation DescriptionLayer
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	DescriptionLayer *layer = [DescriptionLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
        
        CGSize win_size = [[CCDirector sharedDirector] winSize];
        
        float myscale = 0.5;
        if([Display isRetinaDevice]){
            myscale = 1.0;
        }
        
        /* 解説 */
        CCSprite *description = [CCSprite spriteWithFile:@"description.jpg"];
        description.scale = myscale;
        description.position = ccp(win_size.width/2, win_size.height/2);
        [self addChild: description];
        /* 解説 */
        
        /* スタートボタン */
		CCMenuItem *startMenuItem = [CCMenuItemImage itemWithNormalImage:@"start_btn.png" selectedImage:@"start_btn_on.png" target:self selector:@selector(pushStartBtn:)];
        startMenuItem.scale = myscale;
		startMenuItem.position = ccp(win_size.width/2 + 110, win_size.height/2 -100);
        /* スタートボタン */
        
        /* スタートメニュー */
        CCMenu *startMenu = [CCMenu menuWithItems:startMenuItem, nil];
        startMenu.position = CGPointZero;
        [self addChild:startMenu];
    }
    return self;
}

- (void) pushStartBtn:(id)sender{
    [[SimpleAudioEngine sharedEngine] playEffect:@"key_lock.mp3"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameLayer scene] withColor:ccBLACK]];
}

@end
