//
//  Hero.h
//  neoyaji
//
//  Created by kinkori on 13/09/17.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Clowd.h"

@interface Hero : Clowd {
    
}

-(void) returnFace;
-(void) changeFace;
-(void) score_fadeout;

@property int index;
@property(nonatomic, retain) NSArray *clouds;
@property(nonatomic, retain) CCLabelBMFont *score;
@property(nonatomic, retain) NSMutableArray *touched_heros;
@end
