//
//  Dream.m
//  neoyaji
//
//  Created by 蔭山 忠臣 on 2013/10/04.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "Dream.h"
#import "Display.h"


@implementation Dream
int LIFE_DELAY = 4;
float SHOW_DELAY = 1.2;
CGPoint *center;
BOOL is35inch = NO;

-(id) init {
    float myscale = 0.5;
    if([Display isRetinaDevice]){
        myscale = 1.0;
    }
    
    self.scale = myscale;
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle pathForResource:@"heros" ofType:@"plist"];
    NSString* score_path = [bundle pathForResource:@"scores" ofType:@"plist"];
    clouds = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:path]];
    NSArray *scores = [NSArray arrayWithContentsOfFile:score_path];
    int release_h_index = [[NSUserDefaults standardUserDefaults] integerForKey:@"released_hero"];
    if(!release_h_index) {
        release_h_index = 1;
        [[NSUserDefaults standardUserDefaults] setInteger:release_h_index forKey:@"released_hero"];
    }
    /* 開放されているヒーローの番号を取得する */
    [clouds retain];
    h_index = arc4random() % (release_h_index + 1);
    if (h_index == release_h_index + 1) {
        h_index--;
    }
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     [NSString stringWithFormat:@"%@.plist", clouds[h_index]]];
    if(self = [super initWithFile:@"dream_board.png"]) {
        
        hero = [[CCSprite alloc] init];
        hero = [hero initWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:[NSString stringWithFormat:@"%@_%d.png",clouds[h_index], 0]]];
        hero.position = ccp(self.contentSize.width/2, self.contentSize.height/2 + 5);
        if(myscale == 0.5){
            hero.scale = 2;
        }
        [self addChild:hero];
        
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(changeFace) name:@"changeFace" object:nil];
        NSNotificationCenter *nt = [NSNotificationCenter defaultCenter];
        [nt addObserver:self selector:@selector(removeself) name:@"removeself" object:nil];
        /*
         [self schedule:@selector(check_is_out) interval:1.0]
         ;
         */
        score = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%@", scores[h_index]] fntFile:@"number_hade.fnt"];
        score.position = ccp(self.contentSize.width/2, self.contentSize.height/2 - 20);
        score.visible = NO;
        score.scale = 0.5;
        [score retain];
        [self addChild:score];

        self.visible = NO;
        self.opacity = 0;
        CGSize win_size = [[CCDirector sharedDirector] winSize];
        if(win_size.width < 500){
            self.scale = myscale * 0.8;
            is35inch = YES;
        }
        [self showAnimate];
    }
    return self;
}

-(void) setDefaultPosition{
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    self.position = ccp(win_size.width/2 - 30, win_size.height/2);
}

-(void) showAnimate{
    [self setDefaultPosition];
    float default_scale = self.scale;
    self.scale = default_scale * 0.1;
    
    CCCallFunc *remove = [CCCallFunc actionWithTarget:self selector:@selector(removeself)];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:LIFE_DELAY];
    CCShow *show = [CCShow action];
    
    ccBezierConfig bezier;
    CGPoint next_posi = [self getRandomPosition];
    bezier.controlPoint_1 = self.position;
    bezier.controlPoint_2 = ccp(next_posi.x, self.position.y);
    bezier.endPosition = next_posi;
    CCBezierTo *curve = [CCBezierTo actionWithDuration:SHOW_DELAY bezier:bezier];

    CCFadeIn *fin = [CCFadeIn actionWithDuration:SHOW_DELAY];
    CCScaleTo *sto = [CCScaleTo actionWithDuration:SHOW_DELAY scale:default_scale];
    CCSpawn *spawn = [CCSpawn actions:fin, curve, sto, nil];
    CCSequence *seq = [CCSequence actions:show, spawn, delay, remove, nil];
    CCFadeIn *fin2 = [fin copy];
    [hero runAction:fin2];
    [self runAction:seq];
}


-(CGPoint) getRandomPosition{
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    int randX, randY;
    
    if(!is35inch){
        float oyaji_x = win_size.width / 2 - 40;
        float oyaji_width_half = 65.250000000;
        float oyaji_height_half = 93.25;
        float oyaji_head_position = 85.750000;
        float self_width = [self boundingBox].size.width;
        float self_height = [self boundingBox].size.height;
        
        
        int index = arc4random() % 3;
        if (index > 2) {
            index = 2;
        }
        
        
        if(index == 0) {
            randX = arc4random() % (int)(oyaji_x - oyaji_width_half);
            if (randX < self_width/2) {
                randX += self_width/2;
            }
            randY = arc4random() % (int)win_size.height/2 + (int)win_size.height/2;
        }
        
        else if(index == 1) {
            randX = arc4random() % (int)(oyaji_width_half * 2) + (int)(oyaji_x - oyaji_width_half);
            randY = arc4random() % (int)oyaji_head_position + ((int)win_size.height / 2 - 19 + oyaji_height_half);
        }
        
        else {
            randX = arc4random() % (int)(win_size.width / 2 - 40 + oyaji_width_half) + (int)(win_size.width - (win_size.width / 2 - 40 + oyaji_width_half));
            if (win_size.width - randX < self_width/2) {
                randX -= self_width/2;
            }
            
            randY = arc4random() % (int)win_size.height/2 + (int)win_size.height/2;
        }
        
        if(self_height/2 < win_size.height - randY) {
            randY -= self_height/2;
        }
    }else{
        CGPoint posi = [self get35inchPosition];
        randX = posi.x;
        randY = posi.y;
    }
    
    if(randX < win_size.width/2 - 30){
        self.flipX=YES;
    }

    return ccp(randX, randY);
}

-(CGPoint) get35inchPosition{
     CGSize win_size = [[CCDirector sharedDirector] winSize];
    int index = arc4random() % 2;
    int randX, randY;
    float halfW = self.contentSize.width/2;
    float halfH = self.contentSize.height/2;
    int oyajiMaxX = 234;
    int oyajiMaxY = 265;
    if(index < 1){
        randX = arc4random() % (int)(win_size.width - halfW*2) + (int)halfW;
        randY = arc4random() % (int)(win_size.height - oyajiMaxX - (int)halfH) + oyajiMaxX;
    }else{
        randX = arc4random() % (int)(win_size.width - oyajiMaxY - halfW*2) + oyajiMaxY + (int)halfW;
        randY = arc4random() % (int)(win_size.height - (int)halfH) + (int)halfH;
    }
    return ccp(randX, randY);
}

-(void) setRandomPosition{
   }

-(void) changeFace {
    [touched_heros addObject:clouds[h_index]];
    /* GameLayerと共有しているNSMutableArrayに、オブジェクトを加える */
    [self stopAllActions];
    [hero stopAllActions];
    NSMutableArray *anim=[NSMutableArray arrayWithCapacity:1];
    [anim addObject:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:[NSString stringWithFormat:@"%@_1.png", clouds[h_index]]]];
    CCAnimation *animation=[CCAnimation animationWithSpriteFrames:anim delay:0.1];
    
    CCRepeat *repeat = [CCRepeat actionWithAction:[CCAnimate actionWithAnimation:animation restoreOriginalFrame:YES] times:10];
    CCCallFunc *fdo_func = [CCCallFunc actionWithTarget:self selector:@selector(score_fadeout)];
    CCSpawn *spawn = [CCSpawn actions:repeat, fdo_func, nil];
    CCCallFunc *func = [CCCallFunc actionWithTarget:self selector:@selector(returnFace)];
    [hero runAction:[CCSequence actions:spawn, func, nil]];
    NSDictionary *dic = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:[score.string intValue]] forKey:@"score"];
    NSNotification *n = [NSNotification notificationWithName:@"score_add_from_hero" object:self userInfo:dic];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}

-(void) returnFace {
    //[self schedule:@selector(slide)];
    [self removeself];
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    CCSpriteFrame* frame = [cache spriteFrameByName:[NSString stringWithFormat:@"%@_0.png", clouds[h_index]]];
    [hero setDisplayFrame:frame];
}

-(void)score_fadeout {
    score.visible = YES;
    score.opacity = 255;
    CCFadeOut *fadeout = [CCFadeOut actionWithDuration:0.4f];
    CCMoveBy *moveby = [CCMoveBy actionWithDuration:0.4f position:ccp(0, 30)];
    CCSpawn *spawn = [CCSpawn actions:fadeout, moveby, nil];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:0.9f];
    CCMoveBy *movebyreturn = [CCMoveBy actionWithDuration:0.1f position:ccp(0, -30)];
    CCSequence *seq = [CCSequence actions:delay, spawn, movebyreturn, nil];
    //スコアのフェードアウト
    score.visible = YES;
    [score runAction:seq];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

-(void) removeself {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self unscheduleAllSelectors];
    [self removeFromParentAndCleanup:YES];
}

@synthesize touched_heros;

@end
