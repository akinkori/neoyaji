//
//  Hero.m
//  neoyaji
//
//  Created by kinkori on 13/09/17.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "Hero.h"
#import "Display.h"


@implementation Hero

-(id) init {
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle pathForResource:@"heros" ofType:@"plist"];
    NSString* score_path = [bundle pathForResource:@"scores" ofType:@"plist"];
    clouds = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:path]];
    NSArray *scores = [NSArray arrayWithContentsOfFile:score_path];
    int release_index = [[NSUserDefaults standardUserDefaults] integerForKey:@"released_hero"];
    if(!release_index) {
        release_index = 1;
        [[NSUserDefaults standardUserDefaults] setInteger:release_index forKey:@"released_hero"];
    }
    /* 開放されているヒーローの番号を取得する */
    [clouds retain];
    index = arc4random() % (release_index + 1);
    if (index == release_index + 1) {
        index--;
    }
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     [NSString stringWithFormat:@"%@.plist", clouds[index]]];
    if(self = [super initWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:[NSString stringWithFormat:@"%@_%d.png",clouds[index], 0]]]) {
        [self setRandomSpeed];
        [self schedule:@selector(slide)];//スケジューラの呼び出し
    }
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(changeFace) name:@"changeFace" object:nil];
    NSNotificationCenter *nt = [NSNotificationCenter defaultCenter];
    [nt addObserver:self selector:@selector(removeself) name:@"removeself" object:nil];
    
     [self schedule:@selector(check_is_out) interval:1.0];
    score = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%@", scores[index]] fntFile:@"number_hade.fnt"];
    score.position = ccp(self.contentSize.width/2, -20);
    score.visible = NO;
    score.scale = 0.5;
    [score retain];
    [self addChild:score];
    
    return self;
}

-(void) setRandomSpeed{
    speed = (float)rand() / RAND_MAX * 2 + 0.3f;
}

-(void) setOutside{
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    int outX = -1 * self.contentSize.width;
    int randY = arc4random() % (int)win_size.height/3 + ((int)win_size.height/3 * 2);
    self.position = ccp(outX, randY);
}

-(void) changeFace {
    [touched_heros addObject:clouds[index]];
    /* GameLayerと共有しているNSMutableArrayに、オブジェクトを加える */
    [self unschedule:@selector(slide)];
    NSMutableArray *anim=[NSMutableArray arrayWithCapacity:1];
    [anim addObject:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:[NSString stringWithFormat:@"%@_1.png", clouds[index]]]];
    CCAnimation *animation=[CCAnimation animationWithSpriteFrames:anim delay:0.1];
    
    CCRepeat *repeat = [CCRepeat actionWithAction:[CCAnimate actionWithAnimation:animation restoreOriginalFrame:YES] times:10];
    CCCallFunc *fdo_func = [CCCallFunc actionWithTarget:self selector:@selector(score_fadeout)];
    CCSpawn *spawn = [CCSpawn actions:repeat, fdo_func, nil];
    CCCallFunc *func = [CCCallFunc actionWithTarget:self selector:@selector(returnFace)];
    [self runAction:[CCSequence actions:spawn, func, nil]];
    NSDictionary *dic = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:[score.string intValue]] forKey:@"score"];
    NSNotification *n = [NSNotification notificationWithName:@"score_add_from_hero" object:self userInfo:dic];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}

-(void) returnFace {
    [self schedule:@selector(slide)];
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    CCSpriteFrame* frame = [cache spriteFrameByName:[NSString stringWithFormat:@"%@_0.png", clouds[index]]];
    [self setDisplayFrame:frame];
}

-(void)score_fadeout {
    score.visible = YES;
    score.opacity = 255;
    CCFadeOut *fadeout = [CCFadeOut actionWithDuration:0.4f];
    CCMoveBy *moveby = [CCMoveBy actionWithDuration:0.4f position:ccp(0, 30)];
    CCSpawn *spawn = [CCSpawn actions:fadeout, moveby, nil];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:0.9f];
    CCMoveBy *movebyreturn = [CCMoveBy actionWithDuration:0.1f position:ccp(0, -30)];
    CCSequence *seq = [CCSequence actions:delay, spawn, movebyreturn, nil];
    //スコアのフェードアウト
    score.visible = YES;
    [score runAction:seq];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

-(void) removeself {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self unscheduleAllSelectors];
    [self removeFromParentAndCleanup:YES];
}

@synthesize index;
@synthesize clouds;
@synthesize score;
@synthesize touched_heros;

@end
