//
//  GameLayer.m
//  neoyaji
//
//  Created by kinkori on 13/08/24.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import "StartLayer.h"
#import "GameLayer.h"
#import "Display.h"
#import "NoseBalloon.h"
#import "Oyaji.h"
#import "SimpleAudioEngine.h"
#import "Clowd.h"
#import "Twitter/Twitter.h"
#import "GAI.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "Hero.h"
#import "ReleaseLayer.h"
#import "Dream.h"

#define kMEDIA_CODE  @"idxxxxxxxxxoWgn"


@implementation GameLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLayer *layer = [GameLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
    
	// return the scene
	return scene;
}

-(id) init
{
    self.isTouchEnabled = NO;
    if( (self=[super init]) ) {
        int play_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"get_count"];
        if(!play_count) {
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"get_count"];
        }
        /* 毎回のプレイに応じてのゴール目標に使われる保存場所を全てクリアする */
        touched_heros = [[NSMutableArray alloc] initWithCapacity:2];
        [touched_heros retain];
        
        each_score = [NSMutableArray array];
        [each_score retain];
        
        
        id tracker = [[GAI sharedInstance] defaultTracker];
        
        // This screen name value will remain set on the tracker and sent with
        // hits until it is set to a new value or to nil.
        [tracker set:kGAIScreenName
               value:@"ノーマルモード"];
        
        [tracker send:[[GAIDictionaryBuilder createAppView] build]];
        
        
        sky_lis = @[@"bk_morning_sky.jpg", @"bk_noon_sky.jpg", @"bk_evening_sky.jpg"];
        [sky_lis retain];
        bkg_lis = @[@"bk_morning.png", @"bk_noon.png", @"bk_evening.png"];
        [bkg_lis retain];
        bgm_lis = @[@"Town_morning.wav", @"Town_afternoon.mp3", @"evening.wav"];
        [bgm_lis retain];
        stage_index = 0;
        //背景画像のリスト、ステージのインデックス初期か
        
        win_size = [[CCDirector sharedDirector] winSize];
        myscale = 0.5;
        if([Display isRetinaDevice]){
            myscale = 1.0;
        }
        
        /* レイヤーの配置 */
        sky_layer = [CCLayer node];
        [self addChild:sky_layer];
        
        clowd_layer = [CCLayer node];
        [self addChild:clowd_layer];
        
        hero_layer = [CCLayer node];
        [self addChild:hero_layer];
        
        bkg_layer = [CCLayer node];
        [self addChild:bkg_layer];
        
        /* レイヤーの配置 */
        
        /* 空の配置 */
        sky = [[CCSprite alloc] initWithFile:sky_lis[stage_index]];
        sky.position = ccp(win_size.width / 2, win_size.height / 2);
        sky.scale = myscale;
        [sky_layer addChild:sky];
        /* 空の配置 */

        /* 虹の配置 */
        rainbow = [CCSprite spriteWithFile:@"rainbow.png"];
        [clowd_layer addChild:rainbow];
        rainbow.anchorPoint = ccp(0, 1);
        rainbow.position = ccp(0, win_size.height);
        rainbow.visible = NO;
        /* 虹の配置 */
        /* 飛行機雲の配置 */
        plane_cloud = [CCSprite spriteWithFile:@"plane_cloud.png"];
        [clowd_layer addChild:plane_cloud];
        plane_cloud.anchorPoint = ccp(0, 1);
        plane_cloud.position = ccp(0, win_size.height);
        plane_cloud.visible = NO;
        /* 飛行機雲の配置 */
        /* 花火の配置 */
        firework = [CCSprite spriteWithFile:@"firework.png"];
        [clowd_layer addChild:firework];
        firework.anchorPoint = ccp(0, 1);
        firework.position = ccp(0, win_size.height);
        firework.visible = NO;
        /* 花火の配置 */
        /* 花火の配置 */
        color_balloon = [CCSprite spriteWithFile:@"color_balloon.png"];
        [clowd_layer addChild:color_balloon];
        color_balloon.anchorPoint = ccp(0, 1);
        color_balloon.position = ccp(0, win_size.height);
        color_balloon.visible = NO;
        /* 花火の配置 */
        
        
        /* 雲の配置 */
        for(int i=0; i<5; i++){
            Clowd *clowd = [[Clowd alloc] init];
            [clowd setRandomPosition];
            [clowd_layer addChild:clowd];
        }
        /* 雲の配置 */
        
        /* 背景の配置 */
        bkg = [[CCSprite alloc] initWithFile:bkg_lis[stage_index]];
        bkg.position = ccp(win_size.width / 2, win_size.height / 2);
        bkg.scale = myscale;
        [bkg_layer addChild:bkg];
        /* 背景の配置 */
        
        /* ボーナス表示 */
        timing = [[CCSprite alloc] initWithFile:@"good.png"];
        timing.anchorPoint = ccp(0.5, 1);
        timing.position = ccp(win_size.width/2, win_size.height/2 - 100);
        timing.visible = NO;
        //timing.scale = myscale;
        [self addChild:timing];
        /* ボーナス表示 */
        
        /* おやじの配置 */
        oyaji_layer = [CCLayer node];
        [self addChild:oyaji_layer];
        oyaji = [[Oyaji alloc] init];
        oyaji.position = ccp(win_size.width / 2 - 40, win_size.height / 2 - 19);
        oyaji.scale = myscale;
        [oyaji_layer addChild:oyaji];
        /* おやじの配置 */
        
        dream_layer = [CCLayer node];
        [self addChild:dream_layer];
        /* 夢レイヤの配置 */
        
        /* 集中線の配置 */
        closeupline = [CCSprite spriteWithFile:@"shutyu.png"];
        closeupline.scale = myscale;
        closeupline.position = ccp(win_size.width/2, win_size.height/2);
        closeupline.opacity = 0;
        [self addChild:closeupline];
        /* 集中線の配置 */
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(next_game:) name:@"next_game" object:nil];
        
        score_bkg_ = @"point_stage.png";
        [score_bkg_ retain];
        /* 点数表示用の背景を生成 */
        point_bkg = [CCSprite spriteWithFile:score_bkg_];
        point_bkg.anchorPoint = ccp(0.5, 0.5);
        point_bkg.scale = myscale;
        point_bkg.position = ccp(60, win_size.height/2);
        point_bkg.opacity = 0;
        [self addChild:point_bkg];
        /* 点数表示用の背景を生成 */
        
        /* 点数表示用のラベルを生成 */
        label = [[CCLabelBMFont labelWithString:@"0" fntFile:@"number_hade.fnt"] retain];
        label.anchorPoint = ccp(0.5, 0.5);
        label.scale = 0.5;
        label.position = ccp(60, win_size.height/2);
        label.opacity = 0;
        [self addChild:label];
        /* 点数表示用のラベルを生成 */
        
        /* BGMの再生 */
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:bgm_lis[stage_index] loop:YES];
        /* BGMの再生 */
        cloud_interval = 2.0;
        [self schedule:@selector(addClowd) interval:cloud_interval];//スケジューラの呼び出し
    }
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(score_add_from_hero:) name:@"score_add_from_hero" object:nil];
    return self;
}

-(void) next_game:(NSNotification *)center {
    [self set_balloon];
}

-(void) pass_time {
    stage_index += 1;
    [self time_pass];
}

-(void) addClowd{
    Clowd *clowd = [[Clowd alloc]init];
    [clowd setOutside];
    [clowd_layer addChild:clowd];
}

-(void) addDream{
    Dream *dream = [[Dream alloc] init];
    dream.touched_heros = touched_heros;
    [dream_layer addChild:dream];
}

-(void) addHero {
    Hero *hero = [[Hero alloc] init];
    hero.touched_heros = touched_heros;
    [hero setOutside];
    [hero_layer addChild:hero];
}

-(void) showSkyAnimeate:(CCSprite *)effect{
    effect.visible = YES;
    effect.opacity = 255;
    CCShow *show = [CCShow action];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:0.8];
    CCFadeOut *fout = [CCFadeOut actionWithDuration:0.3];
    CCSequence *seq = [CCSequence actions:show, delay, fout, nil];
    [effect runAction:seq];
}


-(CGRect)rect{
    return CGRectMake(-self.contentSize.width/2, -self.contentSize.height/2, self.contentSize.width, self.contentSize.height);
}

-(void)onEnter{
    [super onEnter];
    /* おやじアクビ */
    [oyaji akubi];
    /* おやじアクビ */
}

-(void)onExit{
    [self unscheduleUpdate];
    [super onExit];
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
        float balloon_size = [balloon scale];
        float grid_size = [grid scale];
        [self unschedule:@selector(timer)];
        [self unschedule:@selector(addDream)];
        playing_game = NO;
        [self scoreing:balloon_size grid:grid_size];
        [self.balloon break_balloon];
    NSNotification *n = [NSNotification notificationWithName:@"changeFace" object:self];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}


-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    self.isTouchEnabled = NO;
    [grid removeFromParentAndCleanup:YES];
}

-(void)timer {
    if(playing_game) {
            int seconds = 1;
            float speed = 0.1;
            if(progress_timer.percentage == 0) {
                [self unschedule:@selector(timer)];
            }
            progress_timer.percentage -= speed;
    }
}

-(NSString *)get_bonus:(float) remain {
    
    NSString *str = [[NSString alloc] init];
 
    if(remain > 80) {
        str = @"hyperbonus.png";
        
    }
    
    else if (remain > 50) {
        str = @"superbonus.png";
        
    }
    
    else if (remain > 20) {
        str = @"bonus.png";
    }
    
    else {
        str = @"nobonus.png";
    }
    
    return str;
}

-(void) scoreing:(float)balloon_size grid:(float)grid_size {
    
    BOOL is_big = NO;
    CCSprite *effect;
    
    if(balloon_size > grid_size) {
        is_big = YES;
    }
    float difference = fabs(balloon_size - grid_size);
    //bonus画像を用意してから。
    CCMoveBy *moveby = [CCMoveBy actionWithDuration:1.0f position:ccp(0, 30)];
    CCFadeOut *fadeout = [CCFadeOut actionWithDuration:1.0f];
    CCSpawn *spawn = [CCSpawn actions:moveby, fadeout, nil];
    CCSpawn *spawn_2 = [spawn copy];
    //同時に動かすためにコピーした。
    NSString *just_balloon_str = [[NSString alloc] init];
    
    int hero_count = 0;
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    if (is_big) {
        [oyaji shakiri];
        score_bkg_ = @"point_stage.png";
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"get_bonus" action:@"button_press" label:@"is_big" value:nil] build]];
    }else if(difference < 0.01) {
        [oyaji hypershakiri];
        just_balloon_str = @"timing_3.png";
        score_bkg_ = @"point_stage_rainbow.png";
        hero_count = 20;
        [self showSkyAnimeate:rainbow];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"get_bonus" action:@"button_press" label:@"is_rainbow" value:nil] build]];
    }
    else if (difference < 0.03) {
        [oyaji metyashakiri];
        just_balloon_str = @"timing_2.png";
        hero_count = 15;
        score_bkg_ = @"point_stage_gold.png";
        [self showSkyAnimeate:firework];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"get_bonus" action:@"button_press" label:@"is_gold" value:nil] build]];
    }
    else if (difference < 0.05) {
        [oyaji tyoushakiri];
        just_balloon_str = @"timing_1.png";
        hero_count = 10;
        score_bkg_ = @"point_stage_silver.png";
        [self showSkyAnimeate:plane_cloud];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"get_bonus" action:@"button_press" label:@"is_silver" value:nil] build]];
    }
    else {
        [oyaji shakiri];
        just_balloon_str = @"timing_0.png";
        hero_count = 5;
        score_bkg_ = @"point_stage_bronze.png";
        [self showSkyAnimeate:color_balloon];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"get_bonus" action:@"button_press" label:@"is_bronze" value:nil] build]];
    }
    
    
    [self showScore];
    
    [self showCloseupLine];
    
    [timing setTexture:[[CCTextureCache sharedTextureCache] addImage:just_balloon_str]];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:1.5f];
    CCCallFunc *fnc = [CCCallFunc actionWithTarget:self selector:@selector(pass_time)];
    CCSequence *seq = [CCSequence actions:spawn, delay, fnc, nil];
    [timing runAction:seq];
    [self add_score:difference big:is_big];
}

-(void) showScore{
    CCShow *show = [CCShow action];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:1.6f];
    CCHide *hide = [CCHide action];
    CCSequence *anim = [CCSequence actions:show, delay, hide, nil];
    CCSequence *_anim = [anim copy];
    [point_bkg setTexture:[[CCTextureCache sharedTextureCache] addImage: score_bkg_]];
    point_bkg.opacity = 255;
    label.opacity = 255;
    [point_bkg runAction:anim];
    [label runAction:_anim];
}

-(void) add_score:(float)difference big:(BOOL)is_big {
    int result;
    if(difference < 0.01) {
        result = 1000;
    }
    else if (difference < 0.015) {
        result = 980;
    }
    else if (difference < 0.020) {
        result = 945;
    }
    else if (difference < 0.03) {
        result = 910;
    }
    else if (difference < 0.04) {
        result = 860;
    }
    else if (difference < 0.05) {
        result = 830;
    }
    else if (difference < 0.1) {
        result = 770;
    }
    else if (difference < 0.2) {
        result = 740;
    }
    
    else if (difference < 0.3) {
        result = 700;
    }
    else if (difference < 0.4) {
        result = 630;
    }
    else if (difference < 0.5) {
        result = 540;
    }
    else if (difference < 0.7) {
        result = 470;
    }
    else if (difference < 0.8) {
        result = 250;
    }
    else if (difference < 0.9) {
        result = 190;
    }
    else if (difference < 1.0) {
        result = 50;
    }
    if(0 > result) {
        result = 0;
    }
    if(is_big) {
        result /= 2;
    }
    score += result;
    [label setString:[NSString stringWithFormat:@"%d", result]];

    [each_score addObject:[NSString stringWithFormat:@"%d", result]];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

-(void) set_balloon {
    /* 鼻ちょうちんグリッドの設置 */
    grid = [[Grid alloc] init];
    grid.position = ccp(win_size.width / 2 -40, win_size.height / 2 - 5);
    [self addChild:grid];
    
    
    /* 鼻ちょうちんの配置 */
    balloon = [[NoseBalloon alloc] init:stage_index];
    balloon.scale = myscale * balloon.scale;
    balloon.position = ccp(win_size.width / 2 -40, win_size.height / 2 - 5);
    [self addChild:balloon];
    /* 鼻ちょうちんの配置 */
    
    [self schedule:@selector(addDream) interval:2];//スケジューラの呼び出し
    
    playing_game = YES;
    self.isTouchEnabled = YES;
}

-(void) time_pass {
    if(stage_index >= [bkg_lis count]) {
        [self gameover];
    }
    else {
        CCSprite *next_sky = [CCSprite spriteWithFile:sky_lis[stage_index]];
        [self transitionFromSprite:sky toSprite:next_sky inLayer:sky_layer];
        sky = next_sky;
        CCSprite *next_bkg = [CCSprite spriteWithFile:bkg_lis[stage_index]];
        [self transitionFromSprite:bkg toSprite:next_bkg inLayer:bkg_layer];
        bkg = next_bkg;
        [oyaji akubi];
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:bgm_lis[stage_index] loop:YES];
        //次のゲームへ。
    }
}

-(void) transitionFromSprite:(CCSprite *)sprite toSprite:(CCSprite *)next_sprite inLayer:(CCLayer *)layer{
    next_sprite.scale = sprite.scale;
    next_sprite.position = sprite.position;
    next_sprite.opacity = 255;
    [layer addChild: next_sprite];
    CCFadeIn *fadein = [CCFadeIn actionWithDuration:1.5];
    [next_sprite runAction:fadein];
}

-(void) gameover {
    [[NSUserDefaults standardUserDefaults] setObject:(NSArray *)each_score forKey:@"each_score"];
    [[NSUserDefaults standardUserDefaults] setInteger:score forKey:@"now_get_count"];
    NSLog(@"すこあ%d", score);
    int play_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"get_count"];
    [[NSUserDefaults standardUserDefaults] setInteger:play_count+1 forKey:@"get_count"];
    [[NSUserDefaults standardUserDefaults] setObject:(NSArray *)touched_heros forKey:@"touched_heros"];
    /* プレイ回数とスコアをセット */
    
    
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self unscheduleAllSelectors];
    win_size = [[CCDirector sharedDirector] winSize];
    CCSprite *res_bkg = [CCSprite spriteWithFile:@"result_bkg.jpg"];
    res_bkg.scale = myscale;
    res_bkg.position = ccp(win_size.width / 2, win_size.height / 2);
    res_bkg.opacity = 255;
    [self addChild:res_bkg];
    CCFadeIn *fadein = [CCFadeIn actionWithDuration:1.5];
    CCCallFunc *fnc = [CCCallFunc actionWithTarget:self selector:@selector(setresultMenu)];
    CCSequence *seq = [CCSequence actions:fadein, fnc, nil];
    [res_bkg runAction:seq];
    [[SimpleAudioEngine sharedEngine] playEffect:@"walk.wav"];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"fall_street1.wav" loop:YES];
    NSNotification *n = [NSNotification notificationWithName:@"removeself" object:self];
    [[NSNotificationCenter defaultCenter] postNotification:n];
    [[AidAd agentForMedia:kMEDIA_CODE]showDialog];
}

-(void) showCloseupLine{
    closeupline.opacity = 255;
    CCShow *show = [CCShow action];
    CCHide *hide = [CCHide action];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:0.7];
    CCSequence *anim = [CCSequence actions:show, delay, hide, nil];
    [closeupline runAction:anim];
}

-(void) setresultMenu {
    [self setResultScore];
    
    CCMenuItem *restartMenuItem = [CCMenuItemImage itemFromNormalImage:@"retry_btn.png" selectedImage:@"retry_btn_on.png" target:self selector:@selector(retry)];
    restartMenuItem.scale = myscale;
    
    CCMenuItem *giveupItem = [CCMenuItemImage itemFromNormalImage:@"title_btn.png" selectedImage:@"title_btn_on.png" target:self selector:@selector(giveup)];
    giveupItem.scale = myscale;
    
    /* Tweetボタン */
    CCMenuItem *tweetItem = [CCMenuItemImage itemWithNormalImage:@"tw_btn.png" selectedImage:@"tw_btn.png"
                                                          target:self selector:@selector(postTweet)];
    
    /* Lineボタン */
    CCMenuItem *lineItem = [CCMenuItemImage itemWithNormalImage:@"line_btn.png" selectedImage:@"line_btn.png" target:self selector:@selector(postLine)];
    
    tweetItem.scale = myscale;
    lineItem.scale = myscale;
    tweetItem.position = ccp(50, win_size.height/2 + 90);
    lineItem.position = ccp(win_size.width - 50,  win_size.height/2 + 90);
    restartMenuItem.position = ccp(90, 45);
    giveupItem.position = ccp(win_size.width - 90, restartMenuItem.position.y);
    
    CCMenu *GameOverMenu = [CCMenu menuWithItems: restartMenuItem, giveupItem, tweetItem, lineItem, nil];
    GameOverMenu.position = CGPointZero;
    [self addChild:GameOverMenu];
    
    [self check_next_hero];
}

-(void) setResultScore{
    CCSprite *score_bkg = [CCSprite spriteWithFile:@"score_table.png"];
    score_bkg.anchorPoint = ccp(0.5f, 1);
    score_bkg.scale = myscale;
    score_bkg.position = ccp(win_size.width/2, win_size.height - 20);
    [self addChild:score_bkg];
    

    CCLabelBMFont *score_label = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%d", score] fntFile:@"number_hade.fnt"];
    score_label.anchorPoint = ccp(1, 1);
    score_label.position = ccp(win_size.width/2 + 50, win_size.height - 45);
    score_label.scale = 0.5;
    [self addChild:score_label];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int highscore = [defaults integerForKey:@"highscore"];
    if (!highscore) {
        highscore = 0;
    }
    GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:@"neoyaji"] autorelease];
    scoreReporter.value = score;
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
        if (error != nil)
        {
            [defaults setInteger:score forKey:@"missed_highscore"];
            // 報告エラーの処理
        }
        else {
        }
    }];
    
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle pathForResource:@"syougou" ofType:@"plist"];
    NSArray*  syougou_list = [NSArray arrayWithContentsOfFile:path];
    int syougou_index = (int) (score / 100);
    CCLabelTTF *syougou = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@", syougou_list[syougou_index]] dimensions:CGSizeMake(250, 160) alignment:UITextAlignmentLeft fontName:@"Arial-BoldMT" fontSize:11];
    syougou.anchorPoint = ccp(0.5, 1);
    syougou.position = ccp(win_size.width/2, win_size.height - 120);
    syougou.color = ccc3(0, 0, 0);
    [self addChild:syougou];
    
    if(score > highscore) {
        [defaults setInteger:score forKey:@"highscore"];
        highscore = score;
        
        CCSprite *new_record = [CCSprite spriteWithFile:@"new_record.png"];
        new_record.anchorPoint = ccp(0.5f, 1);
        new_record.scale = myscale;
        new_record.position = ccp(win_size.width/2 - 120, win_size.height - 10);
        [self addChild:new_record];
    }

    CCLabelTTF *highscore_label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", highscore] fontName:@"Arial-BoldMT" fontSize:20];
    highscore_label.anchorPoint = ccp(1, 1);
    highscore_label.position = ccp(win_size.width/2 + 50, win_size.height - 160);
    highscore_label.color = ccc3(0, 0, 0);
    [self addChild:highscore_label];

}

-(void) giveup {
    [[SimpleAudioEngine sharedEngine] playEffect:@"key_lock.mp3"];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[StartLayer scene] withColor:ccBLACK]];
    [[AidAd agentForMedia:kMEDIA_CODE]showDialog];
}

-(void) retry {
    [[SimpleAudioEngine sharedEngine] playEffect:@"key_lock.mp3"];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameLayer scene] withColor:ccBLACK]];
}

//LINEの投稿
- (void) postLine{
    UIImage *screenShot = [self takeGrabScreenImage];
    BOOL installedLine = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"line://"]];
    if (installedLine) { // LINEアプリが存在する
        NSURL *targetUrl;
        if (screenShot != nil) {
            // 画像セット
            //UIPasteboard *pasteboard = [UIPasteboard pasteboardWithName:@"jp.naver.linecamera.pasteboard" create:YES];
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            [pasteboard setData:UIImageJPEGRepresentation(screenShot, 0.8) forPasteboardType:@"public.jpeg"];
            NSString *urlString = [NSString stringWithFormat:@"line://msg/image/%@", pasteboard.name];
            targetUrl = [NSURL URLWithString:urlString];
        }
        id tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"button_press" label:@"line_push" value:nil] build]];
        //LINEに投稿
        
        // LINEアプリを開く
        [[UIApplication sharedApplication] openURL:targetUrl];
        
    } else { // LINEアプリが存在しない
        CCLOG(@"LINE is not installed");
        // LINE App Store へ飛ばす
        // アラートビューを出すなど...
    }
}

//ツイッターの投稿
- (void) postTweet{
    
    SLComposeViewController *twclass = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    // TWTweetComposeViewControllerのインスタンスを生成
    viewController = [[UIViewController alloc] init];
    // デフォルト文字列をセット
    [twclass setInitialText:[NSString stringWithFormat:@""]];
    // 添付画像用のスクリーンキャプチャ
    UIImage *screenShot = [self takeGrabScreenImage];
    // デフォルト画像をセット
    [twclass addImage:(UIImage *)screenShot];
    //アプリURLをセット
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/jp/app/oyaji-megane/id673545276?l=ja&ls=1&mt=8"];
    [twclass addURL:(NSURL *)url];
    
    // つぶやき入力画面を表示
    twclass.completionHandler = ^(SLComposeViewControllerResult result) {
        if(result == SLComposeViewControllerResultDone) {
            // "送信"した場合
            // ルーレット回す
        } else if(result == SLComposeViewControllerResultCancelled) {
            // "キャンセルした場合"
        }
        [viewController dismissViewControllerAnimated:YES completion:nil];
    };
    
    [[[CCDirector sharedDirector] view] addSubview:viewController.view];
    [viewController presentViewController:twclass animated:YES completion:nil];
}

-(UIImage *)takeGrabScreenImage{
    
    [CCDirector sharedDirector].nextDeltaTimeZero = YES;
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    CCRenderTexture* rtx = [CCRenderTexture renderTextureWithWidth:winSize.width height:winSize.height];
    [rtx beginWithClear:0 g:0 b:0 a:1.0f];
    [[[CCDirector sharedDirector] runningScene] visit];
    [rtx end];
    
    return [rtx getUIImage];
}


-(void) check_next_hero {
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle pathForResource:@"heros" ofType:@"plist"];
    NSMutableArray *heros = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:path]];
    /* ヒーローが入っているplistを取得します。これの、count - 1 がrelease_indexの最高値になるはずです。 */
    int max_hero = [heros count] -1;
    int release_index = [[NSUserDefaults standardUserDefaults] integerForKey:@"released_hero"];
    /* 現在開放されているヒーローのindex */
    if(max_hero > release_index) {
        ReleaseLayer *releaselayer = [[ReleaseLayer alloc] init:release_index+1];
        releaselayer.ignoreAnchorPointForPosition = NO;
        releaselayer.anchorPoint = ccp(0.5, 0.5);
        releaselayer.position = ccp(win_size.width/2, win_size.height/2);
        [self addChild:releaselayer];
    }
}

-(void) score_add_from_hero:(NSNotification *)center {
    int add_score = [[[center userInfo] objectForKey:@"score"]intValue];
    score += add_score;
}


@synthesize progress_timer;
@synthesize oyaji;
@synthesize balloon;
@synthesize bkg;
@synthesize bkg_lis;
@synthesize sky_lis;
@synthesize bgm_lis;
@synthesize grid;
@synthesize timeframe;
@synthesize label;
@synthesize stage_label;
@synthesize hero_layer;
@synthesize touched_heros;
@synthesize each_score;
@synthesize score_bkg_;
@synthesize dream_layer;

@end
