//
//  Grid.m
//  neoyaji
//
//  Created by kinkori on 13/08/28.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "Grid.h"
#import "Display.h"


@implementation Grid

-(id) init {
    if(self = [super initWithFile:@"wakusen.png"]) {
        self.anchorPoint = ccp(0.022, 0.542);
        [self set_scale];
    }
    return self;
}

-(void) set_scale {
    
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    float myscale = 0.5;
    if([Display isRetinaDevice]){
        float myscale = 1.0;
    }
 
    double grid_size = 4.0 + arc4random() % 5;
    
    self.scale = grid_size * 0.1 * myscale;

}

@end
