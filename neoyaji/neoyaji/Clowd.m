//
//  Clowd.m
//  neoyaji
//
//  Created by 蔭山 忠臣 on 2013/09/14.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "Clowd.h"
#import "Display.h"


@implementation Clowd

-(id) init {
    NSArray *clouds = [NSArray arrayWithObjects: @"clowd_0_noon.png", @"clowd_1_noon.png", @"clowd_2_noon.png", nil];
    int index = arc4random() % 3;
    if(self = [super initWithFile:clouds[index]]) {
        float myscale = 0.5;
        if([Display isRetinaDevice]){
            myscale = 1.0;
        }
        self.scale = myscale;
        [self setRandomSpeed];
        [self schedule:@selector(slide)];//スケジューラの呼び出し
    }
    
    [self schedule:@selector(check_is_out) interval:1.0];
    
    return self;
}

-(void) setOutside{
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    int outX = -1 * self.contentSize.width;
    int randY = arc4random() % (int)win_size.height/2 + (int)win_size.height/2;
    self.position = ccp(outX, randY);
}

-(void) setRandomPosition{
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    int randX = arc4random() % (int)win_size.width;
    int randY = arc4random() % (int)win_size.height/2 + (int)win_size.height/2;
    self.position = ccp(randX, randY);
}

-(void) setRandomSpeed{
    speed = ((float)rand() / RAND_MAX) * 0.6 + 0.1f;
}

-(void) slide{
    self.position = ccp(self.position.x + speed, self.position.y);
}

-(void)check_is_out {
    CGSize win_size = [[CCDirector sharedDirector] winSize];
    BOOL out = NO;
    float selfposi = self.position.x - (self.contentSize.width/2);
    if(selfposi > win_size.width) {
        out = YES;
    }
    if(out) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self unscheduleAllSelectors];
        [self removeFromParentAndCleanup:YES];
    }
}

@end
