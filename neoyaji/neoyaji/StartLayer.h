//
//  StartLayer.h
//  oyajimegane
//
//  Created by 蔭山 忠臣 on 2013/07/06.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import <GameFeatKit/GFView.h>
#import <GameFeatKit/GFController.h>
#import <GameFeatKit/GFIconController.h>

@interface StartLayer : CCLayer <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate, GFViewDelegate>
{
    float myscale;
    GFIconController *gfIconController;
   }

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;
@property (nonatomic, retain) GFIconView *iconView;
@property (nonatomic, retain) GFIconView *iconView2;

@end
