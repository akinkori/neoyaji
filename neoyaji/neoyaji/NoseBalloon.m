//
//  NoseBalloon.m
//  neoyaji
//
//  Created by kinkori on 13/08/24.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "NoseBalloon.h"
#import "SimpleAudioEngine.h"


@implementation NoseBalloon

-(id)init:(int)index {
    
    if(self = [super initWithFile:@"fusen.png"]) {
        self.anchorPoint = ccp(0.022, 0.542);
        self.scale = 0.1;
        CCSequence *sequence = [self get_sequence:index];
        id repeat = [CCRepeatForever actionWithAction:sequence];
        CCSpeed* speed = [CCSpeed actionWithAction:repeat speed:1.0f];
        speed.tag = 10;
        [self runAction:speed];
        float change_time = (arc4random() % 10 + 1) / 10;
        [self schedule:@selector(changeSpeed) interval:change_time];
    }
    
    return self;
}

-(void)changeSpeed {
    {
        float i = (arc4random()%30+1)/10.0f;
        CCSpeed *speed = (CCSpeed*)[self getActionByTag:10];
        speed.speed = i;
    }
}

-(CCSequence *)get_sequence:(int)index {
    CCScaleBy *scale = [CCScaleBy actionWithDuration:2 scale:10];
    id delay = [CCDelayTime actionWithDuration:0.02];
    id reverse = [scale reverse];
    id start_delay = [CCDelayTime actionWithDuration:0.03];
    CCSequence *sequence = [CCSequence actions:scale, reverse, nil];
    
    return sequence;
}

-(void) break_balloon {
    CCAnimation *animation = [CCAnimation animation];
    [animation addSpriteFrameWithFilename:@"patin_l.png"];
    animation.delayPerUnit = 0.01;
    animation.loops = 1;
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
    CCFadeOut *fadeout = [CCFadeOut actionWithDuration:0.5];
    CCCallFunc *func = [CCCallFunc actionWithTarget:self selector:@selector(remove_balloon)];
    CCSequence *sequence = [CCSequence actions:animate, fadeout, func, nil];
    [self runAction:sequence];
    [[SimpleAudioEngine sharedEngine] playEffect:@"BurstC01@11.wav"];
    [[SimpleAudioEngine sharedEngine] playEffect:@"BurstD01@11.wav"];
    
}

-(void) remove_balloon {
    [self unscheduleAllSelectors];
    [self removeFromParentAndCleanup:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void) dealloc
{
	[super dealloc];
}

@end
