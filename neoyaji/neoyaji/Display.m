//
//  Display.m
//  oyajimegane
//
//  Created by 蔭山 忠臣 on 2013/07/06.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "Display.h"


@implementation Display

+ (BOOL) isRetinaDevice {
    if (![UIScreen instancesRespondToSelector:@selector(scale)] ) {
		return NO;
	}
    
	if ([[UIScreen mainScreen] scale] == 2.0) {
        //Retinaの場合
		return YES;
	} else if ([[UIScreen mainScreen] scale] == 1.0) {
        //Retinaではない場合
        return NO;
    }
    
    return NO;
}
@end
