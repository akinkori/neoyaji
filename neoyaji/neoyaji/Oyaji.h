//
//  Oyaji.h
//  neoyaji
//
//  Created by kinkori on 13/08/25.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Oyaji : CCSprite {
    
}

-(void) akubi;
-(void) shakiri;
-(void) tyoushakiri;
-(void) metyashakiri;
-(void) hypershakiri;
-(id)init;

@end
