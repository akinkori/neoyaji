//
//  Oyaji.m
//  neoyaji
//
//  Created by kinkori on 13/08/25.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "Oyaji.h"


@implementation Oyaji

-(id) init {
    if(self = [super initWithFile:@"oyaji_akubi_0.png"]) {
        
    }
    return self;
}

-(void) akubi {
    CCAnimation *animation = [CCAnimation animation];
    [animation addSpriteFrameWithFilename:@"oyaji_akubi_1.png"];
    [animation addSpriteFrameWithFilename:@"oyaji_akubi_2.png"];
    [animation addSpriteFrameWithFilename:@"oyaji_akubi_0.png"];
    [animation addSpriteFrameWithFilename:@"oyaji_neteru.png"];
    animation.delayPerUnit = 1.6;
    animation.loops = 1;
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
    CCCallFunc *func = [CCCallFunc actionWithTarget:self selector:@selector(make_balloon)];
    CCSequence *sequence = [CCSequence actions:animate, func, nil];
    [self runAction:sequence];
}

-(void) shakiri{
    CCAnimation *animation = [CCAnimation animation];
    [animation addSpriteFrameWithFilename:@"oyaji_yaruki_me.png"];
    [animation addSpriteFrameWithFilename:@"oyaji_yaruki.png"];
    animation.delayPerUnit = 1.2;
    animation.loops = 1;
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
    [self runAction:animate];
}

-(void) tyoushakiri{
    CCAnimation *animation = [CCAnimation animation];
    [animation addSpriteFrameWithFilename:@"oyaji_tyou_yaruki_me.png"];
    [animation addSpriteFrameWithFilename:@"oyaji_tyou_yaruki.png"];
    animation.delayPerUnit = 1.2;
    animation.loops = 1;
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
    [self runAction:animate];
    
}

-(void) metyashakiri{
    CCAnimation *animation = [CCAnimation animation];
    [animation addSpriteFrameWithFilename:@"oyaji_metya_yaruki_me.png"];
    [animation addSpriteFrameWithFilename:@"oyaji_metya_yaruki.png"];
    animation.delayPerUnit = 1.2;
    animation.loops = 1;
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
    [self runAction:animate];
    
}

-(void) hypershakiri{
    CCAnimation *animation = [CCAnimation animation];
    [animation addSpriteFrameWithFilename:@"oyaji_hyper_yaruki_me.png"];
    [animation addSpriteFrameWithFilename:@"oyaji_metya_yaruki.png"];
    animation.delayPerUnit = 1.2;
    animation.loops = 1;
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
    [self runAction:animate];
    
}

-(void) make_balloon {
    NSNotification *n = [NSNotification notificationWithName:@"next_game" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:n];
    
}

-(id)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

@end
